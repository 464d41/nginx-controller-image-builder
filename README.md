# NGINX Controller Image Builder
Builds NGINX Controller image in a public cloud.
## Usage
This is a template repo. Fork it and set variables to let pipeline to build an image in your public cloud account.  
Pipeline flow:
- Create temporary cloud instance
- Copy NGINX controller installer from cloud object storage (AWS S3, GCP Cloud Storage, ...)
- Install controller software 
- Transform instance to an image and store in cloud account
### Steps to build your image
- Get NGINX Controller installer from F5
- Put installer to public cloud object storage
- Fork this repo
- Set variables (see .env file for details)
- Let the pipeline to create an image
## Public Cloud Support Roadmap
- AWS (Supported)
- GCP (Future)
- Azure (Future)
