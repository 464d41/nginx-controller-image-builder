######################################################################
# Allow root login
######################################################################
sudo cp /root/.ssh/authorized_keys /root/.ssh/authorized_keys.bak
sudo sed -i -r 's/.*(ssh-rsa.*)/\1/' /root/.ssh/authorized_keys
